@extends('layouts.app')

@section('content')

<div class="container-fluid m-5">
    <h3 class="m-5 text-center">Search for a movie from the <a href="http://www.omdbapi.com/" target="_blank">OMDb API</a> and add it to your Database</a></h3>

    <div class="container d-flex justify-content-center">
        <form class="form-inline float-right" action="{{route('addMovie')}}">
            <fieldset>    
                <div class="input-group">
                        <input id="search" name="search" type="text" class="form-control" aria-label="Saisie de mots clés" required="required">
                    <div class="input-group-append">
                        <button class="btn btn-info" type="submit">Search</button>
                    </div>
                </div>  
            </fieldset> 
        </form>
    </div>
</div>
@endsection