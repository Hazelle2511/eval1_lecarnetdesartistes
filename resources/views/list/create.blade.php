@extends('layouts.app')

@section('content')
<br>
<a href="/list" class="btn btn-info float-right mr-5">Back</a>
<div class="col-lg-4 col-lg-offset-4 mx-auto">
    <h1>Create New Topic</h1>
    <form class="form-horizontal" action="/list" method="POST">
    @csrf
      <fieldset>
        <div class="form-group" >
            <div class="col-lg-10">
                <!-- <label for="exampleInputTopic" class="form-label">Topic</label> -->
                <textarea id="textarea" cols="30" name="content" rows="10" class="form-control"></textarea>
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
      </fieldset>

  
   </form>
</div>






@endsection