@extends('layouts.app')

@section('content')

<br>
<a href="{{ url('/list/create') }}" class="btn btn-info float-right mr-5">Add New</a>



<div class="col-lg-4 col-lg-offset-4 text-center mx-auto ">
    <h1>Lists of Subjects</h1>

    <ul class="list-group ">
    @foreach($subjectss as $subjects)
        <li class="list-group-item">
       {{$subjects->content}}
       <br>
      
       
       
       
                @if($subjects->status == '1')
                <span style="color:red"> - Treated</span>
                @else
                <span style="color:green"> - Available</span> 
                @endif

       </li>
    </ul>
    @endforeach
</div>

@endsection