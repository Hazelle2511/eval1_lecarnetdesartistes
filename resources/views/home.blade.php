@extends('layouts.app')


@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Join the rap battle</div>
                <!-- <button type="button" class="btn btn-outline-secondary">Add a topic</button> -->
                <a href={{"list/create"}} class="btn btn-dark">Add a topic</a>
                <!-- <button type="button" class="btn btn-outline-secondary">Roll it</button> -->
                <a href={{"roll"}} class="btn btn-dark">Roll it</a>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div><div class="m-3">
            
                 </div>


                    @endif

                    
                </div>
            </div>
        </div>
    </div>
    
</div>
   <div class="container">
                <div class="row">
                    @section('body')
                    @show
                </div>
            
    </div>

@endsection
