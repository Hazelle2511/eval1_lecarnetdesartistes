<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\subjects;
use Illuminate\Support\Facades\DB;



class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subjectss = subjects::all();
        return view ('list.list', compact('subjectss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('list.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request->all();
        $subjects = new subjects;
        $subjects->content = $request->content;
        $subjects->save();
        return redirect('list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // return "Hello";
    }

    public function random() {
        // $subjects = Subjects::all()-> random(1)[0] -> content;
        // return view("roll", ["subjects" => $subjects]);
        // return subjects::all()->random(1);

        
    $subjects = Subjects::where('status', false) -> get();
 
    $count = count($subjects) - true;
    // return $count;

    // // // return view('roll', ['subjects -> $subjects'] );
    $random = random_int(false, $count);
    // return $random;
    $topic = $subjects[$random];
    // return $topic;
    $id = $topic -> id;
    // return $id;

    // //update to db
    DB::update('update subjects set status = true where id =' .$id );
    return view("roll", ["topic" => $topic]);
   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
