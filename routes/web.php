<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function () {
    return view('welcome');
});






Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');

Route::get('/roulette', function () {
    return view('roulette');
});

Route::get('/roll', [App\Http\Controllers\SubjectsController::class, 'random']);
// Route::resource('/list', 'SubjectsController');
Route::get('/list', [App\Http\Controllers\SubjectsController::class, 'index'])->name('list');

Route::get('/list/create', [App\Http\Controllers\SubjectsController::class, 'create'])->name('create');

Route::post('/list', [App\Http\Controllers\SubjectsController::class, 'store'])->name('store');
// Route::post('/list', [App\Http\Controllers\SubjectsController::class, 'store'])->name('home');