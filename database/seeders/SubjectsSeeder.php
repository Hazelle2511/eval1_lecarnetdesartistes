<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('subjects')->insert([
            'subjects' => 'UserClient',
            'email' => 'client@gmail.com',
            'password' => Hash::make('userclient'),
            'created_at' => date('Y-m-d H:i:s'),
            'role' => 'client',
        ]);
    }
}
