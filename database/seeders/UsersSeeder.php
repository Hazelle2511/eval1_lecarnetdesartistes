<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'UserClient',
            'email' => 'client@gmail.com',
            'password' => Hash::make('userclient'),
            'created_at' => date('Y-m-d H:i:s'),
            'role' => 'client',
        ]);
        DB::table('users')->insert([
            'name' => 'UserAdmin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('administrator'),
            'created_at' => date('Y-m-d H:i:s'),
            'role' => 'admin',
        ]);
    }
}
